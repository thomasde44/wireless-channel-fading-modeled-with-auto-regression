% Program to simulate rayleigh fading using a p-th order autoregressive model AR(p) according to 
% "Autoregressive modeling for fading channel simulation", IEEE Transaction on Wireless Communications, July 2005.
function [channel_coeff] = rayleigh_fading(P,M,fm,Ns,epselon)
    % P: ar model order
    % M: number of samples
    % fd: maximum doppler frequency in Hz
    % Ns: number of samples per second
    % epselonn: added bias, depends on the Doppler rate, see the paper "Autoregressive modeling for fading channel simulation". 
   
    % usage: Rayleigh_fading(100,10000,150,3000,0.00000001)
    
   
    %-------------------------------------------------------------------------------------------------------------------------   
     
        for p=1:P+1
          % Bessel autocorrelation function 
          vector_corr(p)=besselj(0,2*pi*fm*(p-1)/(Ns)); 
        end
        % adding a small bias, epselonn, to the autocorrelation matrix to overcome the ill conditioning of Yule-Walker equations
        auto_correaltion_matrix=toeplitz(vector_corr(1:P))+eye(P)*epselon
        % Solving the Yule-Walker equations to obtain the model parameters
        ar_parameters=-inv(auto_correaltion_matrix)*vector_corr(2:P+1)' 
        variance=auto_correaltion_matrix(1,1)+vector_corr(2:P+1)*ar_parameters;
        
        % number of samples to filter
        white_noise_samples=20000;
        % Use the function Filter to generate the channel coefficients
        h=filter(1,[1 ar_parameters.'],wgn(M+white_noise_samples,1,10*log10(variance),'complex')); 
        % Ignore the first length white_noise_samples 
        channel_coeff=h(white_noise_samples+1:end,:);  
         
    %------------------------------------------------------------------------------------------------------------------------- 
