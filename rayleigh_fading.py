import numpy as np
import scipy.signal as sig
import scipy.linalg as la
import scipy.special as sp






# ----------------------------------------------------------
# P - ar order
# M - number of samples
# fd - doppler freq
# Ns - samples per sec
# bias - bias value known in advance
# ----------------------------------------------------------

def rayleigh_fading(P, fd, Ns, M, bias):
    tmp = P
    pi = np.pi
    R_tmp = []
    M = M
    
    for i in range(P+2):
        if i > 0:
            term = (2*(pi)*fd*(i-1))/Ns
            R_tmp.append(sp.jn(0, term))
            
    tmp = np.array(R_tmp)
    R = tmp[0:P]
    R2 = tmp[1:P+1]

    print(R2)
    print(len(R2))
        

    R_mat = la.toeplitz(R)
    # print(R_mat)
    eye = np.identity(P, dtype=complex)
    auto_corr_mat = R_mat+(eye*bias)
    inverse_mat = np.linalg.inv(auto_corr_mat)
    ar_coeff = inverse_mat.dot(R2)
    print(ar_coeff)
    variance = auto_corr_mat[0][0] + R2.dot(ar_coeff)
    print()
    noise = np.random.randn(20000) + 1j * np.random.randn(20000)
   
    # print(noise[0])
    print(len(noise))
    
    # lowpass filter
    ans = sig.lfilter(ar_coeff, 1, noise)
    # print result
    print(ans)
    import matplotlib.pyplot as plt
    plt.plot(10*np.log(abs(ans)))
    plt.show()


rayleigh_fading(10, 90, 10, 20000, 0.000000001)

